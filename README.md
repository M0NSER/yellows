# Yellows



## Zadanie

    Prosze zaimplemntuj serwis, który będzie wyliczał wysokość diety na delegacji
        - PL: kwota bazowa 10 PLN. Po 7 dniach kwota wzrasta x2
        - DE: kwota bazowa 50 PLN. Po 7 dniach kwota wzrasta x1.75
        - GB: kwota bazowa 75 PLN. Po 7 dniach kwota wzrasta x4
        - ES: kwota bazowa 150 PLN. Po 3 dniach kwota obniżona o 50PLN. Po kolejnych 2 dniach kwota obniżona jest o -25PLN
        
    Przykłady:
        1. Gdy pracownik wyjeżdża na delegację do Polski na 10 dni, to kwota jego diety wynosić będzie 130PLN
        2. Gdy pracownik wyjedzie do Hiszpanii na 2 dni, kwota jaką uzyska to 300zł
        3. Gdy pracownik wyjedzie do Hiszpanii na 4 dni, kwota jaką uzyska to 500zł
    
    dieta za dzień należy się tylko wtedy, gdy pracownik w danym dniu przebywa minimum 8 godzin w delegacji
    za sobotę i niedzielę nie należy się dieta

## Info
    Mamy serwis DietCalculator, do niego przekazuemy obiekt typu CountryInterface oraz datę od i do delegacji. 
    Mamy 4 klasy implementujące CountryInterface. Każda klasa reprezentuje kraj 
        - PolandCountry
        - GermanyCountry
        - GreatBritainCountry
        - SpainCountry
    Te klasy mają w sobie zawartą logikę związaną z obliczaniem diety. 
    W zadaniu mamy 2 rodzaje logiki, PolandCountry, GermanyCountry, GreatBritainCountry, nazwałem ją MultiplierDietCalculator i wrzuciłem ją do traita, można go użyć w przypadku tworzenia dodatkowych krajów.
    W klasie SpainCountry mamy kolejną logikę MultiplierDietCalculator, ją też wrzucilem do traita, mimo, że występuję w przypadku tylko 1 kraju, ale w przypadku rozrastania sie systemu bedziemy mogli to wykorzystać w inny "kraju"
