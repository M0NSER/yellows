<?php

namespace App\Traits;

use App\Service\CalendarHelper;
use DateTime;

trait DualLossDietCalculatorTrait
{
    public function calculateDiet(DateTime $from, DateTime $to): float
    {
        $period = CalendarHelper::getDayByDayPeriod($from, $to);

        $result = 0.0;

        /** @var DateTime $dt */
        foreach ($period as $key => $dt) {
            if (CalendarHelper::isWeekend($dt)) {
                continue;
            }

            $amount = $this->getBaseAmount();
            if (($key+1) > $this->getFirstStepDietDays()) {
                $amount += $this->getFirstStepAmount();
            }
            if (($key+1) > ($this->getFirstStepDietDays() + $this->getSecondStepDietDays())) {
                $amount += $this->getSecondStepAmount();
            }

            $result += $amount;
        }

        return $result;
    }


    /**
     * @return float
     */
    public function getBaseAmount(): float
    {
        return $this->baseAmount;
    }

    /**
     * @return int
     */
    public function getFirstStepDietDays(): int
    {
        return $this->firstStepDietDays;
    }

    /**
     * @return float
     */
    public function getFirstStepAmount(): float
    {
        return $this->firstStepAmount;
    }

    /**
     * @return int
     */
    public function getSecondStepDietDays(): int
    {
        return $this->secondStepDietDays;
    }

    /**
     * @return float
     */
    public function getSecondStepAmount(): float
    {
        return $this->secondStepAmount;
    }
}