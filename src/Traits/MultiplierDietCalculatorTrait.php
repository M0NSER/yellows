<?php

namespace App\Traits;

use App\Service\CalendarHelper;
use DateTime;

trait MultiplierDietCalculatorTrait
{
    public function calculateDiet(DateTime $from, DateTime $to): float
    {
        $period = CalendarHelper::getDayByDayPeriod($from, $to);

        $result = 0.0;

        /** @var DateTime $dt */
        foreach ($period as $key => $dt) {
            if (CalendarHelper::isWeekend($dt)) {
                continue;
            }

            $amount = $this->getBaseAmount();

            if (($key+1) > $this->getDaysMultiplier()) {
                $amount *= $this->getAmountMultiplier();
            }

            $result += $amount;
        }

        return $result;
    }

    /**
     * @return float
     */
    public function getBaseAmount(): float
    {
        return $this->baseAmount;
    }

    /**
     * @return float
     */
    public function getAmountMultiplier(): float
    {
        return $this->amountMultiplier;
    }

    /**
     * @return int
     */
    public function getDaysMultiplier(): int
    {
        return $this->daysMultiplier;
    }
}