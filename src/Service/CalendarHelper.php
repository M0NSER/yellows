<?php

declare(strict_types=1);

namespace App\Service;

use DateInterval;
use DatePeriod;
use DateTime;

class CalendarHelper
{
    /**
     * @param DateTime $dateTime
     *
     * @return bool
     */
    public static function isWeekend(DateTime $dateTime): bool
    {
        return in_array($dateTime->format('l'), CalendarInterface::WEEKEND_DAYS);
    }

    /**
     * @param DateTime $from
     * @param DateTime $to
     *
     * @return DatePeriod
     */
    public static function getDayByDayPeriod(DateTime $from, DateTime $to): DatePeriod
    {
        $interval = DateInterval::createFromDateString('1 day');

        return new DatePeriod($from, $interval, $to);
    }
}