<?php

namespace App\Service;

interface CalendarInterface
{
    const DAY_SATURDAY = 'Saturday';
    const DAY_SUNDAY = 'Sunday';

    const WEEKEND_DAYS = [
        self::DAY_SATURDAY,
        self::DAY_SUNDAY,
    ];
}