<?php

declare(strict_types=1);

namespace App\Service;

use App\Dto\Country\CountryInterface;
use DateTime;

class DietCalculator implements CalculatorInterface
{
    /**
     * @inheritDoc
     */
    public function calculate(CountryInterface $country, DateTime $from, DateTime $to): float
    {
        return $country->calculateDiet($from, $to);
    }
}