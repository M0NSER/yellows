<?php

namespace App\Service;

use App\Dto\Country\CountryInterface;
use DateTime;

interface CalculatorInterface
{
    /**
     * @param CountryInterface $country
     * @param DateTime         $from
     * @param DateTime         $to
     *
     * @return float
     */
    public function calculate(CountryInterface $country, DateTime $from, DateTime $to): float;
}