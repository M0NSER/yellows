<?php

namespace App\Controller;

use App\Dto\Country\PolandCountry;
use App\Dto\Country\SpainCountry;
use App\Service\CalculatorInterface;
use DateTime;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Routing\Annotation\Route;

class DietController extends AbstractController
{
    /**
     * @var CalculatorInterface
     */
    private CalculatorInterface $dietCalculator;

    public function __construct(
        CalculatorInterface $dietCalculator
    )
    {
        $this->dietCalculator = $dietCalculator;
    }

    /**
     * @Route("", name="app_diet")
     */
    public function index(): JsonResponse
    {
        return $this->json(
            $this->dietCalculator->calculate(
                new SpainCountry(),
                new DateTime("2022-11-07 00:00:00"),
                new DateTime("2022-11-10 23:59:59"),
            )
        );
    }
}
