<?php

declare(strict_types=1);

namespace App\Dto\Country;

use App\Traits\MultiplierDietCalculatorTrait;

class GreatBritainCountry extends Country implements CountryInterface
{
    use MultiplierDietCalculatorTrait;

    private float $baseAmount = 75.0;

    private float $amountMultiplier = 4.0;

    private int $daysMultiplier = 7;

    /**
     * @inheritDoc
     */
    public function getCountrySlug(): string
    {
        return 'gb';
    }
}