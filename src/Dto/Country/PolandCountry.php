<?php

declare(strict_types=1);

namespace App\Dto\Country;

use App\Traits\MultiplierDietCalculatorTrait;

class PolandCountry extends Country implements CountryInterface
{
    use MultiplierDietCalculatorTrait;

    private float $baseAmount = 10.0;

    private float $amountMultiplier = 2.0;

    private int $daysMultiplier = 7;

    /**
     * @inheritDoc
     */
    public function getCountrySlug(): string
    {
        return 'pl';
    }
}