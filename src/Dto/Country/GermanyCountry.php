<?php

declare(strict_types=1);

namespace App\Dto\Country;

use App\Traits\MultiplierDietCalculatorTrait;

class GermanyCountry extends Country implements CountryInterface
{
    use MultiplierDietCalculatorTrait;

    private float $baseAmount = 50.0;

    private float $amountMultiplier = 1.75;

    private int $daysMultiplier = 7;

    /**
     * @inheritDoc
     */
    public function getCountrySlug(): string
    {
        return 'de';
    }
}