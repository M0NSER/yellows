<?php

namespace App\Dto\Country;

use DateTime;

interface CountryInterface
{
    /**
     * @return string
     */
    public function getCountrySlug(): string;

    /**
     * @param DateTime $from
     * @param DateTime $to
     *
     * @return float
     */
    public function calculateDiet(DateTime $from, DateTime $to): float;
}