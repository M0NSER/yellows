<?php

declare(strict_types=1);

namespace App\Dto\Country;

use App\Traits\DualLossDietCalculatorTrait;
use DateTime;

class SpainCountry extends Country implements CountryInterface
{
    use DualLossDietCalculatorTrait;

    private float $baseAmount = 150.0;

    private int $firstStepDietDays = 3;
    private float $firstStepAmount = -50.0;

    private int $secondStepDietDays = 2;
    private float $secondStepAmount = -25.0;

    /**
     * @inheritDoc
     */
    public function getCountrySlug(): string
    {
        return 'es';
    }
}